import java.util.ArrayList;
import java.util.List;

//Given an array of positive ints, return a new array of length "count" containing the first even
//numbers from the original array. The original array will contain at least "count" even numbers.
//
//        copyEvens([3, 2, 4, 5, 8], 2) → [2, 4]
//        copyEvens([3, 2, 4, 5, 8], 3) → [2, 4, 8]
//        copyEvens([6, 1, 2, 4, 5, 8], 3) → [6, 2, 4]

public class CopyEvens {
    public static void main(String[] args) {

        int[] nums = {6, 1, 2, 4, 5, 8};
        int count = 3;
        List<Integer> list = new ArrayList<>();
        for(int i = 0; i < nums.length; i++){
            list.add(nums[i]);
        }
        //input
        System.out.println("input");
        for(int num:list){
            System.out.println(num);
        }

        int i = 0;
        List<Integer> newList = new ArrayList<>();
        while(newList.size() < count && i < list.size()) {
            int value = list.get(i);
            if(value % 2 == 0){
               newList.add(value);
            }
            i++;
        }

        System.out.println("Output");
        for(int num:newList){
            System.out.println(num);
        }

    }
}
